import time


class TmuxIOHandler(object):
    """Interacts with the tmux session: sends commands as inputs to the tmux session and reads the output of the tmux session.

    Attributes:
        tmuxObject: libtmux object.
    """


    def __init__(self, tmuxObject):
        """Inits TmuxIOHandler class with a tmux object."""

        self.pane = tmuxObject.pane


    def sendPing(self):
        """Sends PING command."""

        self.pane.send_keys('ping sat')
        time.sleep(2)

    def readOutput(self):
        """Reads what is printed in the tmux terminal and returns a list which contains each line as a string"""

        time.sleep(2)
        output = self.pane.capture_pane()
        return output


    def checkPing(self):
        """Checks if PONG was received. Returns true if received. """

        output = self.readOutput()

        for line in reversed(output):
            if "pong" in line.lower():
                return True
        return False


    def routing(self, gs, satellite):
        """Sends the routing command to the desired ground station and satellite. Simplified. Depends on how the routing is done. 

        Args:
            gs: str, desired ground station ID
            satellite: str, desired satellite
        """

        routing_options = "route set " + gs + satellite

        self.pane.send_keys(routing_options)
        self.pane.enter()
        time.sleep(2)

    def commsSetup(self, baud_rate, frequency, sat_id):
        """Checks that the csp-doppler has the correct configuration parameters and changes them otherwise"""

        self.pane.send_keys("param list " + sat_id)

        output = self.readOutput()

        for line in output:
            if "baud_rate" in line:
                if not baud_rate in line:
                    self.pane.send_keys("param set baud_rate " + baud_rate)

            if "frequency" in line:
                if not frequency in line:
                    self.pane.send_keys("param set frequency " + frequency)

        # this can be simplified and expanded by using lists. 

    def sendSatelliteHealthChecks(self):
        """Sends a command that asks the satellite to report 5 very important health parameters."""

        self.pane.send_keys('gscript run health_checks.gs')
        self.pane.enter()
        time.sleep(2)


    def checkSatelliteHealthChecks(self):
        """Checks if the health checks have been received. Check if the received params are above thresholds. Returns true if received. """

        health_check_1 = False
        health_check_2 = False
        health_check_3 = False
        health_check_4 = False
        health_check_5 = False

        health_checks_received = False
    
        output = self.readOutput()

        for line in output:
            if "EPS Temperature" in line:
                health_check_1 = True
                # check if the EPS Temp is above 0 Degrees, for example. 
                if float(line.split('=')[1]) >= 0:
                    # do something
                    pass
                else:
                    # do something
                    pass

            if "Battery Voltage" in line:
                health_check_2 = True

                # check if the Battery Voltage is above a certain threshold i.e. 4800mV.
                if int(line.split('=')[1]) >= 4800:
                    # do something
                    pass
                else:
                    # do something
                    pass

            if "OBC Status" in line:
                health_check_3 = True
                # check if the OBC Status is Nominal
                if str(line.split('=')[1]) == 'Nominal':
                    # do something
                    pass
                else:
                    # do something
                    pass

            if "Free Disk Space" in line:
                health_check_4 = True
                # check if the free disk space is above a certain threshold i.e.100MB
                if float(line.split('=')[1]) >= 100000:
                    # do something
                    pass
                else:
                    # do something
                    pass

            if "Watchdog Time Left" in line:
                health_check_5 = True
                # needs to check if the watchdog time is above a certain threshold i.e. 1 day
                if float(line.split('=')[1]) >= 86400:
                    # do something
                    pass
                else:
                    # do something
                    pass
            
        if health_check_1 and health_check_2 and health_check_3 and health_check_4 and health_check_5:
            health_checks_received = True
            
            time.sleep(2)

            # again this can be simplified by using lists instead of one by one checks with a string of ifs.

            return health_checks_received


    def sendFTP(self, ftpCommand, ftpServerNumber):
        """Sends an FTP (file transfer protocol) command. 

        Args:
            ftpCommand: str, ftp command (e.g.: 'ftp download /from/path/file /to/path/file/')
            ftpServerNumber: int, number of the ftp server. Point to the correct subsystem to send the file to. (e.g.: ftp server <number>)
        """

        ftpServer = 'ftp server ' + str(ftpServerNumber)

        self.pane.send_keys(ftpServer)
        time.sleep(1)

        self.pane.send_keys(ftpCommand)
        time.sleep(2)


    def checkFTP(self):
        """Checks FTP command, returns true if received, false if not received. 

        Args:
            ftpCommand: str, ftp command (e.g.: 'ftp download /from/path/file /to/path/file/')
            ftpServerNumber: int, number of the ftp server. Point to the correct subsystem to send the file to. (e.g.: ftp server <number>)

        """

        output = self.readOutput()

        for line in output:
            if "100%" in line: 
                return True

        time.sleep(1)

        return False


    def sendCommand(self, command):
        self.pane.send_keys(command)
