from TmuxIOHandlerClass import TmuxIOHandler
from TmuxObjectClass import TmuxObject
from prepassConfigClass import prepassConfig
import time

class fakeSat(object):
    """This class contains methods to be called from the passOperator to simulate an actual pass
        Attributes:    
            tmux_name: name of the tmux session to connect to.
    """

    def __init__(self, tmux_name):
        """Initiates the fakeSat and connects it to the tmux terminal"""

        self.tmux_name = tmux_name
        self.tmux_handler = TmuxIOHandler(TmuxObject(self.tmux_name))


    def pong(self):
        """Send a pong through the terminal"""

        self.tmux_handler.sendCommand('pong')
        time.sleep(1)
    

    def sendHealthChecks(self):
        """send the health checks params to the GS"""
    
        self.tmux_handler.sendCommand("""
            EPS Temperature (ºC) =  4 
            Battery Voltage (mV) = 5000 
            OBC Status = Nominal 
            Free Disk Space (B) = 200000
            Watchdog Time Left (s) = 90000
            """)
        time.sleep(1)

    def confirmFTP(self, file, download):
        """This method confirms the reception of the file sent by the ground station.
        Args:
            file: name of the file "downloaded" 
            download: True if downloading from the sat, False if uploading to the sat
        """
        if download:
            self.tmux_handler.sendCommand(f"""File {file} downloaded 100%. CRC Check confirmed.""")
        else:
            self.tmux_handler.sendCommand("""File {file} received 100%.""")
        time.sleep(1)

    def commsAnswers(self):
        self.tmux_handler.sendCommand('Changing Configuration...')
        time.sleep(2)
        self.tmux_handler.sendCommand('Configuration changed. Please reboot to apply the new configuration.')

        #keep going
        return
