from TmuxIOHandlerClass import TmuxIOHandler
from TmuxObjectClass import TmuxObject
from prepassConfigClass import prepassConfig
from fakeSat import fakeSat
import time


class passOperator(object):
    """This class operates the pass. Could be further configured by reading a JSON file with the instructions to be made during a certain pass.""" 
    
    def run(self): 
        """This Method connects to the TMUX terminal and contains all the logic related to repeating commands and operating the pass"""

        prepass = prepassConfig()
        prepass.readConfiguration('passConfig.ini')
        prepass.setup()

        self.fake_sat = fakeSat(prepass.tmux_session_name)

        comms_config_file = "/path/to/comms_config_file"

        tmux_handler = TmuxIOHandler(TmuxObject(prepass.tmux_session_name))

        tmux_handler.sendCommand('clear')

        time.sleep(0.5)

        self.ping(tmux_handler)

        self.downloadPlatformData(tmux_handler)

        self.downloadPayloadData(tmux_handler)

        self.changeCommsConfig(tmux_handler, comms_config_file)

    
    def ping(self, tmux_handler):
        """Ping the satellite and wait for a response, if no response is received, send the ping command again.
        Args: 
            tmux_handler: tmux IO object class attached to a running pane. 
        """

        tmux_handler.sendPing()

        while not tmux_handler.checkPing():
            tmux_handler.sendPing()
            self.fake_sat.pong()
            time.sleep(1)


    def downloadPlatformData(self, tmux_handler):
        """Ask the satellite for the platform data, if no data is received, send the command again.
        Args: 
            tmux_handler: tmux IO object class attached to a running pane. 
        """

        while not tmux_handler.checkSatelliteHealthChecks():
            tmux_handler.sendSatelliteHealthChecks()
            self.fake_sat.sendHealthChecks()
            time.sleep(1)
        

    def downloadPayloadData(self, tmux_handler):
        """Ask the satellite for the payload data, if no data is received, send the command again.
        Args: 
            tmux_handler: tmux IO object class attached to a running pane. 
        """

        payload_server_number = 3

        while not tmux_handler.checkFTP():
            tmux_handler.sendFTP('ftp download payload_data', payload_server_number)
            self.fake_sat.confirmFTP('payload data', download=True)
            time.sleep(1)
    

    def changeCommsConfig(self, tmux_handler, comms_config_file):        
        """Upload to the satellite the config file, then point the comms to the config file, 
        restart the subsystem and check if everything went well. If a step fails,
        send the command again.
        Args: 
            tmux_handler: tmux IO object class attached to a running pane. 
        """
        comms_server_number = 2

        while not tmux_handler.checkFTP():
            tmux_handler.sendFTP(f'ftp upload {comms_config_file}', comms_server_number)
            self.fake_sat.confirmFTP(comms_config_file, download=False)

        tmux_handler.sendCommand(f'service {comms_server_number} new config file {comms_config_file}')

        self.fake_sat.commsAnswers()

        # don't have any more time.

        time.sleep(2)
    
        # check that everything is running correctly. 



if __name__ == '__main__':
    operate_pass = passOperator()
    operate_pass.run()