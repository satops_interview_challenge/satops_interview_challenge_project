import libtmux
import time


class TmuxObject(object):
    """Connects to a tmux session.

    Attributes:
        server: libtmux.server.Server object, tmux server
        session: libtmux.Session object, tmux session
        pane: libtmux.pane object, tmux session pane
    """

    def __init__(self, tmuxSessionName):
        """Inits the tmux object with the session name.

        Args:
            tmuxSessionName: str, tmux session name
        """
        self.server = libtmux.Server()
        self.session = self.server.find_where({"session_name": tmuxSessionName})
        self.pane = self.session.attached_pane

    def test_connection(self):

        self.pane.send_keys('echo testing', enter=False)
        self.pane.enter()
        time.sleep(1)

        output = self.pane.capture_pane()
        for line in output:
            if "testing" in line:
                print("connection successful")
                break

