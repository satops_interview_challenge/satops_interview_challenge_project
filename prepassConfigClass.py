import configparser
from TmuxIOHandlerClass import TmuxIOHandler
from TmuxObjectClass import TmuxObject


class prepassConfig(object):
    """Contains the methods that will configure the Ground Station to operate the pass"""


    def __init__(self):
        """Inits the prepassConfig class with certain global variables"""

        self.pass_start = ""
        self.pass_end = ""
        self.ground_station = ""
        self.sat_id = 0
        self.tmux_session_name = ""


    def readConfiguration(self, configFile):
        """Reads the configuration file to setup the pass
        
        Args:
            configFile: path to the configFile of the pass. 
        """
    
        config = configparser.ConfigParser()

        with open(configFile, 'r') as configfile:
            config.read_file(configfile)
            
            self.pass_start = config['PASS_TIMES']['pass_start']
            self.pass_end = config['PASS_TIMES']['pass_end']
            self.ground_station = config['GS']['gs']
            self.tmux_csp_doppler = config['GS']['tmux_csp_dopp']
            self.sat_id = config['SAT']['sat_id']


    def setup(self):
        "Given the name of the satellite and a config table for each satellite, this method sets up the comms"

        config = configparser.ConfigParser()

        with open('satConfig.ini', 'r') as configfile:
            config.read_file(configfile)

            baud_rate = config[self.sat_id]['baud_rate']
            frequency = config[self.sat_id]['frequency']
            self.tmux_session_name = config[self.sat_id]['tmux_session_name']


        tmux_handler = TmuxIOHandler(TmuxObject(self.tmux_session_name))

        tmux_handler.routing(self.ground_station, self.sat_id)

        tmux_csp_doppler = TmuxIOHandler(TmuxObject(self.tmux_csp_doppler))

        tmux_csp_doppler.commsSetup(baud_rate, frequency, self.sat_id)

       


    
        
    